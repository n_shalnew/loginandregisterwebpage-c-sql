﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using HtmlAgilityPack;

namespace LoginApp
{
    public class Crawler
    {
        public string        name = "";
        public string        cost = "";
        public string description = "";
        public string         url = "";

        public Crawler(string url)
        {
            this.url = url;
        }

        public async Task findAll()
        {
            var httpClient = new HttpClient();
            var html = await httpClient.GetStringAsync(url);
            
            var htmlDocument = new HtmlAgilityPack.HtmlDocument { };
            htmlDocument.LoadHtml(html);

            //get Name
            var Product = htmlDocument.DocumentNode.Descendants("div")
                           .Where(node => node.GetAttributeValue("id", "")
                           .Equals("titleSection")).ToList();
            if (Product.Count() > 0)
            {
                name = Product[0].InnerText.Trim();
            }
            else
            {
                name = "emptyName";
            }
            //end get Name

            //get Cost
            var Product1 = htmlDocument.DocumentNode.Descendants("span")
                   .Where(node => node.GetAttributeValue("id", "")
                           .Equals("priceblock_ourprice")).ToList();
            if (Product1.Count() > 0)
            {
                cost = Product1[0].InnerText;
            }
            else
            {
                cost = "emptyCost";
            }
            //end get Cost

            //get Description
            var Product2 = htmlDocument.DocumentNode.Descendants("ul")
           .Where(node => node.GetAttributeValue("class", "")
           .Equals("a-unordered-list a-vertical a-spacing-none")).ToList();
            if (Product2.Count() > 0)
            {
                description = Product2[0].InnerText.Replace("\t", string.Empty);
                description = description.Replace("\n\n\n", string.Empty);
            }
            else
            {
                description = "emptyDescription";
            }
            //end get Description

        }
    }
}