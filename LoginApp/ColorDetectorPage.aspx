﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ColorDetectorPage.aspx.cs" Inherits="LoginApp.ColorDetectorPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Table ID="Table1" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label1" runat="server" Text="Get main webpage color "></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="ReturnToMain" runat="server" Text="Return to parcing" OnClick="ReturnToMain_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:TextBox ID="Input" runat="server"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Output" runat="server" Text="Result will be here"></asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="GetColor" runat="server" Text="Get color" OnClick="GetColor_Click" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </div>
    </form>
</body>
</html>
