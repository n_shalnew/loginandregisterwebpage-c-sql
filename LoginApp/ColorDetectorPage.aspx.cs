﻿using Alx.Web;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginApp
{
    public partial class ColorDetectorPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie login = Request.Cookies["login"];
            HttpCookie sign = Request.Cookies["sign"];
            if (login != null && sign != null)
            {
                if (sign.Value == SignGenerator.GetSign(login.Value + "bytepp"))
                {

                    return;
                }
            }
            Response.Redirect("LoginPage.aspx");
        }
        private void SaveImhFromUrl()
        {//получает изображение по ссылке

            //const string url = "https://www.excalibur.ws/how-to-join";
            string url = Input.Text;
            var device = Devices.Desktop;
            var path = String.Format(@"website-{0}.png", device);
            Alx.Web.Screenshot.Save(url, path, ImageFormat.Png, device);

            /*
            device = Devices.TabletLandscape;
            path = String.Format(@"C:\Users\Nick\Pictures\_work\website-{0}.png", device);
            Alx.Web.Screenshot.Save(url, path, ImageFormat.Png, device);

            device = Devices.PhonePortrait;
            path = String.Format(@"C:\Users\Nick\Pictures\_work\website-{0}.png", device);
            Alx.Web.Screenshot.Save(url, path, ImageFormat.Png, device);
            */
        }
        protected void GetColor_Click(object sender, EventArgs e)
        {
            SaveImhFromUrl();
            string filep = @"website-Desktop.png";

            Bitmap bMap = Bitmap.FromFile(filep) as Bitmap;

            if (bMap == null) throw new FileNotFoundException("Cannot open picture file for analysis");

            PictureAnalysis.GetMostUsedColor(bMap);
            Output.Text = Convert.ToString(PictureAnalysis.MostUsedColor);
        }

        protected void ReturnToMain_Click(object sender, EventArgs e)
        {
            Response.Redirect("UserPage.aspx", false);
        }
    }
}