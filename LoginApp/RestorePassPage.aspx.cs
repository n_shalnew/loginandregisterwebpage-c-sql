﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Mail;
using System.Net;

namespace LoginApp
{
    public partial class RestorePassPage : System.Web.UI.Page
    {
        private SqlConnection sqlConnection = null;
        protected async void Page_Load(object sender, EventArgs e)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            sqlConnection = new SqlConnection(connectionString);
            await sqlConnection.OpenAsync();

        }

        protected async void Button1_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> db = new Dictionary<string, string>();
            SqlCommand getUsersCredCmd = new SqlCommand("SELECT [Login], [Pass] FROM [Users]", sqlConnection);
            SqlDataReader sqlReader = null;
            try
            {
                sqlReader = await getUsersCredCmd.ExecuteReaderAsync();
                while (await sqlReader.ReadAsync())
                {
                    db.Add(Convert.ToString(sqlReader["Login"]), Convert.ToString(sqlReader["Pass"]));
                }
            }
            catch { }
            finally
            {
                if (sqlReader != null)
                {
                    sqlReader.Close();
                }
            }

            if (db.Keys.Contains(TextBox1.Text))
            {
                var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("uintrou.sobaka@gmail.com", "ChoGall2"),
                    EnableSsl = true
                };

                string message
                    = "Hi! here is your password \r\n" 
                    + db[TextBox1.Text] + 
                    "\r\n Don't forget it again!";

                client.Send("uintrou.sobaka@gmail.com", TextBox2.Text, "Restore password", message);

                string script = "alert('Check your em-mail box');";
                ClientScript.RegisterClientScriptBlock(this.GetType(), "MessageBox", script, true);
            }

            Response.Redirect("LoginPage.aspx");
        }
    }
}