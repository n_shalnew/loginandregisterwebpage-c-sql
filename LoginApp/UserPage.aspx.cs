﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LoginApp
{
    public partial class UserPage : System.Web.UI.Page
    {
        private SqlConnection sqlConnection = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie login = Request.Cookies["login"];
            HttpCookie sign = Request.Cookies["sign"];
            if (login != null && sign != null)
            {
                if (sign.Value == SignGenerator.GetSign(login.Value + "bytepp"))
                {
                    Label1.Text = login.Value;
                    LoadFromDB();
                    return;
                }
            }
            Response.Redirect("LoginPage.aspx");
        }

        protected void LogOut_Click(object sender, EventArgs e)
        {
            Response.Redirect("LogoutPage.aspx");
        }

        protected void LoadToDB(string name, string price, string description)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();
            con.Open();
            SqlCommand cmd = new SqlCommand();

            cmd.CommandText = "INSERT INTO [Results] (Name, Price, Description) VALUES (@Name, @Price, @Description)";
            cmd.Connection = con;
            cmd.Parameters.AddWithValue("Name", name);
            cmd.Parameters.AddWithValue("Price", price);
            cmd.Parameters.AddWithValue("Description", description);
            SqlDataReader rd = cmd.ExecuteReader();
            rd.Close();
            con.Close();

        }

        protected void LoadFromDB()
        {
            StringBuilder table = new StringBuilder();
            if (!Page.IsPostBack)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ToString();
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM [Results]";
                cmd.Connection = con;
                SqlDataReader rd = cmd.ExecuteReader();
                table.Append("<table border='1'>");
                table.Append("<tr><th>Name</th><th>Cost</th><th>Description</th></tr>");

                if (rd.HasRows)
                {
                    while (rd.Read())
                    {
                        table.Append("<tr>");
                        table.Append("<th>" + rd[1] + "</td>");
                        table.Append("<th>" + rd[2] + "</td>");
                        table.Append("<th>" + rd[3] + "</td>");
                        table.Append("</tr>");
                    }
                }

                table.Append("</table>");
                PlaceHolder1.Controls.Add(new Literal { Text = table.ToString() });
                rd.Close();
            }

        }

        protected async void ParceBtn_Click(object sender, EventArgs e)
        {
            /*
            string _name = "Motorola Nexus 6 Unlocked GSM Smartphone, 32GB, White (Certified Refurbished)";
            string _cost = "$139.99";
            string _description = "This Certified Refurbished product is tested and certified to look and work like new, with limited to no wear. The refurbishing process includes functionality testing, inspection, and repackaging. headphone and SIM card sold separately.\nMore space to explore -A bigger phone with more everything.\nBrilliant 6 screen and dual front-facing stereo speakers make content shine.\nGet the most out of Android 5.0 Lollipop(compatible with Android Marshmallow 6.0).\nUnlocked Verizon branded nexus 6 cellphone.";
            */
            Crawler crawler = new Crawler(inputUrl.Text);
            await crawler.findAll();

            LoadToDB(crawler.name, crawler.cost, crawler.description);
            //LoadToDB(_name, _cost, _description);
            Response.Redirect("UserPage.aspx", false);

        }

        protected void Page_Unload(object sender, EventArgs e)
        {
            if (sqlConnection != null && sqlConnection.State != ConnectionState.Closed)
            {
                sqlConnection.Close();
            }
        }

        protected void SwitchToDetect_Click(object sender, EventArgs e)
        {
            Response.Redirect("ColorDetectorPage.aspx", false);
        }
    }
}