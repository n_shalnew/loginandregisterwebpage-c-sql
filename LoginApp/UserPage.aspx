﻿<%@ Page Language="C#" AutoEventWireup="true" Async="true" CodeBehind="UserPage.aspx.cs" Inherits="LoginApp.UserPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Table ID="Table1" runat="server" Style="margin: 20px">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="SwitchToDetect" runat="server" Text="DetectColor" OnClick="SwitchToDetect_Click"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:TextBox ID="inputUrl" runat="server" Width="250"></asp:TextBox>
                    </asp:TableCell>

                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="ParceBtn" runat="server" Text="Parce" OnClick="ParceBtn_Click" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Button ID="Button2" runat="server" Text="LogOut" OnClick="LogOut_Click" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>


        </div>
        <div>
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        </div>
    </form>
</body>
</html>
